import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class Card extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		let icon_name = '?';
		let icon_color = '#393939';

		if (this.props.is_open) {
			icon_name = this.props.name;
		}

		return (
			<TouchableOpacity
				style={styles.card}
				onPress={this.props.clickCard}>
				{this.props?.hide ? null :
					<Text
						style={styles.card_text}
					>
						{icon_name}
					</Text>}
			</TouchableOpacity>
		);
	}



}

export default Card;


const styles = StyleSheet.create({
	card: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1,
		borderRadius: 8,
		margin: 4
	},
	card_text: {
		fontSize: 30,
		fontWeight: 'bold',
		textAlign: 'center'
	}
});