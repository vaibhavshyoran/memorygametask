shuffle = (dataArray) => {

    var i = dataArray.length, j, temp;
    if (i == 0) return dataArray;
    while (--i) {
        j = Math.floor(Math.random() * (i + 1));
        temp = dataArray[i];
        dataArray[i] = dataArray[j];
        dataArray[j] = temp;
    }
    return dataArray;
}