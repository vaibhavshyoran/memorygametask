import React from 'react';
import { StyleSheet, View, Button } from 'react-native';
import { Ionicons, FontAwesome, Entypo } from 'react-native-vector-icons'; // 6.2.2

import Header from './components/Header';
import Score from './components/Score';
import Card from './components/Card';

const shuffle = (dataArray) => {

  var i = dataArray.length, j, temp;
  if (i == 0) return dataArray;
  while (--i) {
    j = Math.floor(Math.random() * (i + 1));
    temp = dataArray[i];
    dataArray[i] = dataArray[j];
    dataArray[j] = temp;
  }
  return dataArray;
}

export default class App extends React.Component {

  constructor(props) {
    super(props);

    let sources = {
      'fontawesome': FontAwesome,
      'entypo': Entypo,
      'ionicons': Ionicons
    };

    let cards = [
      {
        name: 'A',
      },
      {
        name: 'B',
      },
      {
        name: 'C',
      },
      {
        name: 'D',
      },
      {
        name: 'E',
      },
      {
        name: 'F',
      },
      {
        name: 'G',
      },
      {
        name: 'H',
      }
    ];

    let clone = JSON.parse(JSON.stringify(cards));

    this.cards = cards.concat(clone);
    this.cards.map((obj) => {
      let id = Math.random().toString(36).substring(7);
      obj.id = id;
      obj.src = sources[obj.src];
      obj.is_open = false;
    });

    this.cards = shuffle(this.cards);
    this.state = {
      current_selection: [],
      selected_pairs: [],
      score: 0,
      cards: this.cards,
      turns: 0,
    }

  }

  

  resetCards() {
    let cards = this.cards.map((obj) => {
      obj.is_open = false;
      return obj;
    });

    cards = shuffle(cards);

    this.setState({
      current_selection: [],
      selected_pairs: [],
      cards: cards,
      turns: 0,
      score: 0
    });
  }


  renderRows() {

    let contents = this.getRowContents(this.state.cards);
    return contents.map((cards, index) => {
      return (
        <View key={index} style={styles.row}>
          { this.renderCards(cards)}
        </View>
      );
    });

  }


  renderCards(cards) {
    return cards.map((card, index) => {
      const hide = this.state.selected_pairs?.includes(card?.name);
      return (
        <Card
          key={index}
          name={card.name}
          is_open={card.is_open}
          hide={hide}
          clickCard={() => this.clickCard(card.id)}
        />
      );
    });
  }


  clickCard = (id) => {
    let selected_pairs = this.state.selected_pairs;
    let current_selection = this.state.current_selection;
    let score = this.state.score;
    let turns = this.state.turns

    let index = this.state.cards.findIndex((card) => {
      return card.id == id;
    });

    let cards = this.state.cards;

    if (cards[index].is_open == false && selected_pairs.indexOf(cards[index].name) === -1) {

      cards[index].is_open = true;

      current_selection.push({
        index: index,
        name: cards[index].name
      });

      if (current_selection.length == 2) {
        turns += 1;
        if (current_selection[0].name == current_selection[1].name) {
          score += 1;
          selected_pairs.push(cards[index].name);
        } else {

          cards[current_selection[0].index].is_open = false;

          setTimeout(() => {
            cards[index].is_open = false;
            this.setState({
              cards: cards
            });
          }, 500);
        }

        current_selection = [];
      }

      this.setState({
        score: score,
        cards: cards,
        turns: turns,
        current_selection: current_selection
      });

    }

  }


  getRowContents(cards) {
    let contents_r = [];
    let contents = [];
    let count = 0;
    cards.forEach((item) => {
      count += 1;
      contents.push(item);
      if (count == 4) {
        contents_r.push(contents)
        count = 0;
        contents = [];
      }
    });

    return contents_r;
  }
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <Score title="Turn" score={this.state.turns} />
        <View style={styles.body}>
          {
            this.renderRows()
          }
        </View>
        <Score title="Matches" score={this.state.score} />
        <Button
          onPress={()=>this.resetCards()}
          title="Reset"
          color="#008CFA"
        />
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff'
  },
  row: {
    flex: 1,
    flexDirection: 'row'
  },
  body: {
    flex: 10,
    justifyContent: 'space-between',
    padding: 10,
    marginTop: 5
  }
});
